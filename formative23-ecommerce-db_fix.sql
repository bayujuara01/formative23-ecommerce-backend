-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               8.0.26 - MySQL Community Server - GPL
-- Server OS:                    Win64
-- HeidiSQL Version:             10.1.0.5464
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for formative23-e-commerce
CREATE DATABASE IF NOT EXISTS `formative23-e-commerce` /*!40100 DEFAULT CHARACTER SET utf8 */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `formative23-e-commerce`;

-- Dumping structure for table formative23-e-commerce.detail_user
CREATE TABLE IF NOT EXISTS `detail_user` (
  `id` int NOT NULL AUTO_INCREMENT,
  `address` varchar(255) DEFAULT NULL,
  `create_time` datetime(6) DEFAULT NULL,
  `delete_time` datetime(6) DEFAULT NULL,
  `fullname` varchar(255) DEFAULT NULL,
  `telepon` varchar(40) DEFAULT NULL,
  `update_time` datetime(6) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb3;

-- Dumping data for table formative23-e-commerce.detail_user: ~0 rows (approximately)
/*!40000 ALTER TABLE `detail_user` DISABLE KEYS */;
INSERT INTO `detail_user` (`id`, `address`, `create_time`, `delete_time`, `fullname`, `telepon`, `update_time`) VALUES
	(1, NULL, '2021-11-06 20:24:27.000000', NULL, 'Bayu', NULL, NULL),
	(2, NULL, '2021-11-06 20:24:37.000000', NULL, 'Harun', NULL, NULL),
	(3, NULL, '2021-11-06 20:24:44.000000', NULL, 'Harun2', NULL, NULL);
/*!40000 ALTER TABLE `detail_user` ENABLE KEYS */;

-- Dumping structure for table formative23-e-commerce.product
CREATE TABLE IF NOT EXISTS `product` (
  `id` int NOT NULL AUTO_INCREMENT,
  `create_time` datetime(6) DEFAULT NULL,
  `delete_time` datetime(6) DEFAULT NULL,
  `desc` longtext,
  `image_url` varchar(255) DEFAULT NULL,
  `price` bigint DEFAULT NULL,
  `product_name` varchar(255) DEFAULT NULL,
  `update_time` datetime(6) DEFAULT NULL,
  `store_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_product_store_idx` (`store_id`),
  KEY `fk_product_store1_idx` (`store_id`),
  CONSTRAINT `FKjlfidudl1gwqem0flrlomvlcl` FOREIGN KEY (`store_id`) REFERENCES `store` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb3;

-- Dumping data for table formative23-e-commerce.product: ~0 rows (approximately)
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
INSERT INTO `product` (`id`, `create_time`, `delete_time`, `desc`, `image_url`, `price`, `product_name`, `update_time`, `store_id`) VALUES
	(1, '2021-11-06 14:51:27.000000', NULL, 'Xiaomi Redmi Note 10 Pro', NULL, 3700000, 'Redmi Note 10 Pro', NULL, 1),
	(2, '2021-11-06 14:52:12.000000', NULL, 'Xiaomi Redmi Note 10', NULL, 3500000, 'Redmi Note 10', '2021-11-06 14:52:37.000000', 1);
/*!40000 ALTER TABLE `product` ENABLE KEYS */;

-- Dumping structure for table formative23-e-commerce.role
CREATE TABLE IF NOT EXISTS `role` (
  `id` int NOT NULL AUTO_INCREMENT,
  `role_name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb3;

-- Dumping data for table formative23-e-commerce.role: ~0 rows (approximately)
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` (`id`, `role_name`) VALUES
	(1, 'USER'),
	(2, 'STORE'),
	(3, 'ADMIN');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;

-- Dumping structure for table formative23-e-commerce.store
CREATE TABLE IF NOT EXISTS `store` (
  `id` int NOT NULL AUTO_INCREMENT,
  `city` varchar(255) NOT NULL,
  `create_time` datetime(6) DEFAULT NULL,
  `delete_time` datetime(6) DEFAULT NULL,
  `store_name` varchar(255) NOT NULL,
  `update_time` datetime(6) DEFAULT NULL,
  `user_id` int NOT NULL,
  `store_id` int NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_bjqth9sjo3phwbmybuysm65be` (`user_id`),
  KEY `FKlvsnmewayiyn5uad7wgbuqomd` (`store_id`),
  CONSTRAINT `FKlvsnmewayiyn5uad7wgbuqomd` FOREIGN KEY (`store_id`) REFERENCES `store` (`id`),
  CONSTRAINT `FKn82wpcqrb21yddap4s3ttwnxj` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3;

-- Dumping data for table formative23-e-commerce.store: ~0 rows (approximately)
/*!40000 ALTER TABLE `store` DISABLE KEYS */;
INSERT INTO `store` (`id`, `city`, `create_time`, `delete_time`, `store_name`, `update_time`, `user_id`, `store_id`) VALUES
	(1, 'Purworejo', '2021-11-06 14:47:07.064886', NULL, 'BubuiHP', NULL, 2, 1);
/*!40000 ALTER TABLE `store` ENABLE KEYS */;

-- Dumping structure for table formative23-e-commerce.transaction
CREATE TABLE IF NOT EXISTS `transaction` (
  `id` int NOT NULL AUTO_INCREMENT,
  `create_time` datetime(6) DEFAULT NULL,
  `grand_total` int DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  `update_time` datetime(6) DEFAULT NULL,
  `user_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_transaction_user1_idx` (`user_id`),
  CONSTRAINT `FKsg7jp0aj6qipr50856wf6vbw1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb3;

-- Dumping data for table formative23-e-commerce.transaction: ~0 rows (approximately)
/*!40000 ALTER TABLE `transaction` DISABLE KEYS */;
INSERT INTO `transaction` (`id`, `create_time`, `grand_total`, `status`, `update_time`, `user_id`) VALUES
	(1, '2021-11-06 20:06:19.507269', 0, 'UNPAID', NULL, 7),
	(2, '2021-11-06 20:07:27.958118', 0, 'PAID', NULL, 7),
	(3, '2021-11-06 20:09:54.310344', 0, 'UNPAID', NULL, 7),
	(4, '2021-11-06 20:12:15.522740', 0, 'UNPAID', NULL, 7);
/*!40000 ALTER TABLE `transaction` ENABLE KEYS */;

-- Dumping structure for table formative23-e-commerce.transaction_detail
CREATE TABLE IF NOT EXISTS `transaction_detail` (
  `id` int NOT NULL AUTO_INCREMENT,
  `create_time` datetime(6) DEFAULT NULL,
  `qty` int DEFAULT NULL,
  `total` int DEFAULT NULL,
  `update_time` datetime(6) DEFAULT NULL,
  `product_id` int NOT NULL,
  `transaction_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKp0202i7eat7u1q5i9sxm1442k` (`product_id`),
  KEY `fk_transaction_detail_transaction1_idx` (`transaction_id`),
  CONSTRAINT `FK2nh7hmi2mfurimsk0viq4a127` FOREIGN KEY (`transaction_id`) REFERENCES `transaction` (`id`),
  CONSTRAINT `FKp0202i7eat7u1q5i9sxm1442k` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb3;

-- Dumping data for table formative23-e-commerce.transaction_detail: ~0 rows (approximately)
/*!40000 ALTER TABLE `transaction_detail` DISABLE KEYS */;
INSERT INTO `transaction_detail` (`id`, `create_time`, `qty`, `total`, `update_time`, `product_id`, `transaction_id`) VALUES
	(2, '2021-11-06 20:10:45.000000', 1, 1, NULL, 1, 1),
	(3, '2021-11-06 20:12:15.551738', 3, 11100000, NULL, 1, 4);
/*!40000 ALTER TABLE `transaction_detail` ENABLE KEYS */;

-- Dumping structure for table formative23-e-commerce.user
CREATE TABLE IF NOT EXISTS `user` (
  `id` int NOT NULL AUTO_INCREMENT,
  `create_time` datetime(6) DEFAULT NULL,
  `delete_time` datetime(6) DEFAULT NULL,
  `password` varchar(120) NOT NULL,
  `update_time` datetime(6) DEFAULT NULL,
  `username` varchar(16) NOT NULL,
  `detail_user_id` int DEFAULT NULL,
  `role_id` int NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_nmc480kbe0m1484x89daqr0i8` (`detail_user_id`),
  KEY `fk_user_detail_user_idx` (`detail_user_id`),
  KEY `fk_user_role1_idx` (`role_id`),
  CONSTRAINT `FK5b0wmueiq1ff2wrfgym2p9vb1` FOREIGN KEY (`detail_user_id`) REFERENCES `detail_user` (`id`),
  CONSTRAINT `FKn82ha3ccdebhokx3a8fgdqeyy` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb3;

-- Dumping data for table formative23-e-commerce.user: ~0 rows (approximately)
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`id`, `create_time`, `delete_time`, `password`, `update_time`, `username`, `detail_user_id`, `role_id`) VALUES
	(2, NULL, NULL, 'a21f8b37e82b3cad8ca91b407a911fad4f1b54c4fabc0d639c1f993ddc2ca5fd', '2021-11-06 14:47:07.015892', 'bayujuara01', 1, 2),
	(7, NULL, NULL, '21b5c9dd1f92c9425da22b0a33bab86b4963053832360f908b5839e9c48e9397', NULL, 'harunjuara01', 2, 1),
	(8, NULL, NULL, '21b5c9dd1f92c9425da22b0a33bab86b4963053832360f908b5839e9c48e9397', NULL, 'harunjuara012', NULL, 1),
	(9, NULL, NULL, 'a21f8b37e82b3cad8ca91b407a911fad4f1b54c4fabc0d639c1f993ddc2ca5fd', NULL, 'bayujuara02', NULL, 1),
	(10, '2021-11-06 20:37:06.411979', NULL, 'a21f8b37e82b3cad8ca91b407a911fad4f1b54c4fabc0d639c1f993ddc2ca5fd', NULL, 'bayujuara04', NULL, 1);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
