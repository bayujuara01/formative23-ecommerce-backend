package com.eariefyantobayu.testecommerce;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@SpringBootApplication
public class TestEcommerceApplication {

    public static void main(String[] args) {
        SpringApplication.run(TestEcommerceApplication.class, args);
    }

    

}
