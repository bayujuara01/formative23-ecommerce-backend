package com.eariefyantobayu.testecommerce.controller;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.eariefyantobayu.testecommerce.dto.CustomUser;
import com.eariefyantobayu.testecommerce.entity.User;
import com.eariefyantobayu.testecommerce.handler.PasswordHashHandler;
import com.eariefyantobayu.testecommerce.handler.ResponseHandler;
import com.eariefyantobayu.testecommerce.repository.UserRepository;
import com.eariefyantobayu.testecommerce.util.response.DataResponse;
import com.eariefyantobayu.testecommerce.util.response.ErrorResponse;

@RestController
@RequestMapping("/api")
public class AuthController {

	@Autowired
	UserRepository userRepository;

	@PostMapping(value = "/auth/login", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<ResponseHandler> checkUserAuthentication(@Valid @RequestBody User user) {
		Optional<CustomUser.GetByUsername> userRequest = userRepository.findOneByUsernameNative(user.getUsername());

		if (userRequest.isPresent()) {
			CustomUser.GetByUsername userResult = userRequest.get();
			String passwordRequestHash = PasswordHashHandler.hashSha256(user.getPassword());

			if (userResult.getPassword().equals(passwordRequestHash)) {
				return ResponseEntity.ok(DataResponse.of(HttpStatus.OK, userResult));
			}
		}

		return ResponseEntity.status(HttpStatus.UNAUTHORIZED)
				.body(ErrorResponse.of(HttpStatus.UNAUTHORIZED, "Unauthorizez user", "Username or password are wrong"));

	}
}
