package com.eariefyantobayu.testecommerce.controller;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.eariefyantobayu.testecommerce.dto.CustomDetailUser;
import com.eariefyantobayu.testecommerce.entity.DetailUser;
import com.eariefyantobayu.testecommerce.handler.ResponseHandler;
import com.eariefyantobayu.testecommerce.repository.DetailUserRepository;
import com.eariefyantobayu.testecommerce.util.response.DataResponse;
import com.eariefyantobayu.testecommerce.util.response.ErrorResponse;

@RestController
@RequestMapping("/api")
public class DetailUserController {
	@Autowired
	DetailUserRepository detailUserRepository;
	
	@GetMapping(value="/detail-users", 
			produces = MediaType.APPLICATION_JSON_VALUE)
		ResponseEntity<ResponseHandler> getAllProduct() {
			List<CustomDetailUser.GetAll> product = detailUserRepository.findAllNative();
			if(product.size()<=0) {
				return ResponseEntity.internalServerError()
					.body(ErrorResponse.of(HttpStatus.INTERNAL_SERVER_ERROR,
					"Internal server error", "List Of Detail user is Empty"));
			} else {
				return ResponseEntity.ok(DataResponse.of(HttpStatus.OK, product, "List of detail user"));
			}
		}	
		
		@GetMapping(value="/user/{uid}/detail-user/{id}", 
			produces = MediaType.APPLICATION_JSON_VALUE)
		ResponseEntity<ResponseHandler> getProductById(@PathVariable int uid, @PathVariable int id) {
			List<CustomDetailUser.GetAll> product = detailUserRepository.findByIdNative(id);
			if(product.size()<=0) {
				return ResponseEntity.internalServerError()
					.body(ErrorResponse.of(HttpStatus.INTERNAL_SERVER_ERROR, 
					"Internal server error", "Detail user is not found"));
			} else {
				return ResponseEntity.ok(DataResponse.of(HttpStatus.OK, product,"Detail user"));
			}
		}
	
	@PostMapping(value="/detail-user", 
			consumes = MediaType.APPLICATION_JSON_VALUE,
			produces = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<ResponseHandler> createProduct(@RequestBody DetailUser payload) {
		int res = detailUserRepository.addNewDetailUserNative(payload.getFullname(), payload.getAddress()
			,payload.getTelepon(), Instant.now());
		if(res ==1) {
			List<CustomDetailUser.GetAll> dataDetail = detailUserRepository.findLastOfListNative();
			return ResponseEntity.ok(DataResponse.of(HttpStatus.OK, dataDetail, "Add detail user successfully"));
		} else {
			return ResponseEntity.internalServerError()
				.body(ErrorResponse.of(HttpStatus.INTERNAL_SERVER_ERROR, 
				"Internal server error", "Add new detail user failed"));
		}
	}
	
	@DeleteMapping(value="/detail-user/{id}", 
		consumes = MediaType.APPLICATION_JSON_VALUE,
		produces = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<ResponseHandler> softDeleteProduct(@PathVariable int id) {
	List<CustomDetailUser.GetAll> dataDetail = detailUserRepository.findByIdDeleteNative(id);
		if(dataDetail.size()>0) {
			int res = detailUserRepository.softDeleteDetailUserNative(Instant.now(), id);
			if(res ==1) {
				List<CustomDetailUser.GetAll> data = detailUserRepository.findByIdDeleteNative(id);
				return ResponseEntity.ok(DataResponse.of(HttpStatus.OK, data ,"Delete Successfully"));
			} else {
				return ResponseEntity.internalServerError()
					.body(ErrorResponse.of(HttpStatus.INTERNAL_SERVER_ERROR, 
					"Internal server error", "Delete failed"));
			}
		} else {
			return ResponseEntity.internalServerError()
				.body(ErrorResponse.of(HttpStatus.INTERNAL_SERVER_ERROR, 
				"Internal server error", "Delete failed"));
		}
	
	}
	
	@PutMapping(value = "/detail-user/{id}", 
		consumes = MediaType.APPLICATION_JSON_VALUE,
		produces = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<ResponseHandler> updateProduct(@PathVariable int id, @RequestBody DetailUser payload) {
		List<CustomDetailUser.GetAll> detailRequest = detailUserRepository.findByIdNative(id);
			
		if (detailRequest.isEmpty()) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND)
				.body(ErrorResponse.of(HttpStatus.NOT_FOUND, 
				"Error Not found", "Detail User not found"));
		} else {
			Integer rowAffected = detailUserRepository.updateByIdNative(payload.getFullname(), payload.getAddress()
				,payload.getTelepon(), Instant.now() ,id);
				List<CustomDetailUser.GetAll> data= detailUserRepository.findByIdNative(id);

			if (rowAffected > 0) {
				return ResponseEntity.ok(DataResponse.of(HttpStatus.OK, data ,String.valueOf(rowAffected)));	
			} else {
				return ResponseEntity.internalServerError()
					.body(ErrorResponse.of(HttpStatus.INTERNAL_SERVER_ERROR, 
					"Internal server error", "Update user error"));
			}	
		}
			
	}
	
}
