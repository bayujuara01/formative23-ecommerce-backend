package com.eariefyantobayu.testecommerce.controller;

import java.io.IOException;

import com.eariefyantobayu.testecommerce.util.FileUploadUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/api")
public class FileController {
    
    @PostMapping("/images/upload")
    ResponseEntity<?> saveImage(@RequestParam("image") MultipartFile multipartFile) throws IOException {
        String fileName = StringUtils.cleanPath(multipartFile.getOriginalFilename());
        FileUploadUtils.saveFile("test-uploads", fileName, multipartFile);
        return ResponseEntity.ok(fileName);
    }
}
