package com.eariefyantobayu.testecommerce.controller;

import java.io.IOException;
import java.time.Instant;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;

import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.eariefyantobayu.testecommerce.dto.CustomProduct;
import com.eariefyantobayu.testecommerce.dto.CustomStore;
import com.eariefyantobayu.testecommerce.entity.Product;
import com.eariefyantobayu.testecommerce.entity.Store;
import com.eariefyantobayu.testecommerce.entity.User;
import com.eariefyantobayu.testecommerce.handler.ResponseHandler;
import com.eariefyantobayu.testecommerce.repository.ProductRepository;
import com.eariefyantobayu.testecommerce.repository.StoreRepository;
import com.eariefyantobayu.testecommerce.repository.UserRepository;
import com.eariefyantobayu.testecommerce.util.FileUploadUtils;
import com.eariefyantobayu.testecommerce.util.response.DataResponse;
import com.eariefyantobayu.testecommerce.util.response.ErrorResponse;

@RestController
@RequestMapping("/api")
public class ProductController {
	@Autowired
	ProductRepository productRepository;

	@Autowired
	UserRepository userRepository;

	@Autowired
	StoreRepository storeRepository;
	
	@GetMapping(value="/products", 
		produces = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<ResponseHandler> getAllProduct() {
		List<CustomProduct.GetAll> product = productRepository.findAllNative();
		if(product.size()<=0) {
			return ResponseEntity.ok(DataResponse.of(HttpStatus.OK, List.of(),"Product empty or not found"));
		} else {
			return ResponseEntity.ok(DataResponse.of(HttpStatus.OK, product));
		}
	}	
	
	@GetMapping(value="/product/{id}", 
		produces = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<ResponseHandler> getProductById(@PathVariable int id) {
		List<CustomProduct.GetAll> product = productRepository.findByIdNative(id);
		if(product.size()<=0) {
			return ResponseEntity.internalServerError()
				.body(ErrorResponse.of(HttpStatus.INTERNAL_SERVER_ERROR, 
				"Internal server error", "Product is not found"));
		} else {
			return ResponseEntity.ok(DataResponse.of(HttpStatus.OK, product));
		}
	}
	
	// image-static/bayujuara01/product/1223045.jpg
	@PostMapping(value="/product", 
		consumes = MediaType.MULTIPART_FORM_DATA_VALUE,
		produces = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<ResponseHandler> createProduct(
		@RequestParam("file") MultipartFile imageFile,
		@RequestParam("name") String name,
		@RequestParam("desc") String desc,
		@RequestParam("price") Long price,
		@RequestParam("user_id") Integer userId
	) throws IOException {
		Optional<User> userRequest = userRepository.findById(userId);

		if (userRequest.isPresent() && userRequest.get().getRole().getId() == 2) {
			Optional<CustomStore.GetAll> store = storeRepository.findByUserIdNative(userId);

			if (store.isPresent()) {
				System.out.println("Store ID : " + store.get().getId());

				
				String fileName = StringUtils.cleanPath(imageFile.getOriginalFilename());
				String userDirectory = userRequest.get().getUsername() + "/product";
				String uploadPath = FileUploadUtils.saveFile(userDirectory, fileName, imageFile);

				int res = productRepository.addNewProductNative(name, price, desc, 
					uploadPath, Instant.now(), store.get().getId());

				if(res > 0) {
					List<CustomProduct.GetAll> dataProducts = productRepository.findLastOfListNative();
					return ResponseEntity.ok(DataResponse.of(HttpStatus.OK, dataProducts));
				} else {
					return ResponseEntity.internalServerError()
						.body(ErrorResponse.of(HttpStatus.INTERNAL_SERVER_ERROR, 
						"Internal server error", "Add new product failed"));
				}
			}
		}

		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ErrorResponse.of(HttpStatus.BAD_REQUEST, "Bad Request Error"));
	}
	
	@DeleteMapping(value="/product/{id}", 
		consumes = MediaType.APPLICATION_JSON_VALUE,
		produces = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<ResponseHandler> softDeleteProduct(@PathVariable int id) {
		int res = productRepository.softDeleteProductNative(Instant.now(),id);
		if(res <1) {
			return ResponseEntity.internalServerError()
					.body(ErrorResponse.of(HttpStatus.INTERNAL_SERVER_ERROR, 
					"Internal server error", "Add new product failed"));
		} else {
			List<CustomProduct.GetAll> dataProducts = productRepository.findByIdDeleteNative(id);
			return ResponseEntity.ok(DataResponse.of(HttpStatus.OK, dataProducts));
			
		}
	}
	
	@PutMapping(value = "/product/{id}", 
		consumes = MediaType.APPLICATION_JSON_VALUE,
		produces = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<ResponseHandler> updateProduct(@PathVariable int id, @RequestBody Product product) {
		List<CustomProduct.GetAll> productRequest = productRepository.findByIdNative(id);
		
		if (productRequest.isEmpty()) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND)
				.body(ErrorResponse.of(HttpStatus.NOT_FOUND, 
				"Error Not found", "Product not found"));
		} else {
			Integer rowAffected = productRepository.updateById(product.getProductName(), product.getPrice()
				,product.getDesc(), product.getImageUrl(), Instant.now() ,id);
			List<CustomProduct.GetAll> data= productRepository.findByIdNative(id);

			if (rowAffected > 0) {
				return ResponseEntity.ok(DataResponse.of(HttpStatus.OK, data ,""));	
			} else {
				return ResponseEntity.internalServerError()
					.body(ErrorResponse.of(HttpStatus.INTERNAL_SERVER_ERROR, 
					"Internal server error", "Update user error"));
			}	
		}
		
	}
	
	@PostMapping(value="/product/search", 
		consumes = MediaType.APPLICATION_JSON_VALUE,
		produces = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<ResponseHandler> searchProduct(@RequestParam String productName,
		@RequestParam String desc , @RequestParam int price ) {
		List<CustomProduct.GetAll> poductRequest= productRepository.findByParameter(productName,
				desc, price );
		if(poductRequest.size()>0) {
			List<CustomProduct.GetAll> dataProducts = productRepository.findLastOfListNative();
			return ResponseEntity.ok(DataResponse.of(HttpStatus.OK, dataProducts));
		} else {
			return ResponseEntity.ok(ErrorResponse.of(HttpStatus.INTERNAL_SERVER_ERROR, "Internal server error", "Add new product failed"));
		}
	}

}
