package com.eariefyantobayu.testecommerce.controller;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import com.eariefyantobayu.testecommerce.dto.CreateStoreDto;
import com.eariefyantobayu.testecommerce.dto.CustomProduct;
import com.eariefyantobayu.testecommerce.dto.CustomStore;
import com.eariefyantobayu.testecommerce.dto.StoreProductDto;
import com.eariefyantobayu.testecommerce.entity.Store;
import com.eariefyantobayu.testecommerce.entity.User;
import com.eariefyantobayu.testecommerce.handler.ResponseHandler;
import com.eariefyantobayu.testecommerce.repository.ProductRepository;
import com.eariefyantobayu.testecommerce.repository.StoreRepository;
import com.eariefyantobayu.testecommerce.repository.UserRepository;
import com.eariefyantobayu.testecommerce.util.response.DataResponse;
import com.eariefyantobayu.testecommerce.util.response.ErrorResponse;

@RestController
@RequestMapping("/api")
public class StoreController {

	@Autowired
	StoreRepository storeRepository;

	@Autowired
	UserRepository userRepository;

	@Autowired
	ProductRepository productRepository;
	
	@GetMapping(value = "/stores", 
		produces = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<ResponseHandler> getAllStore() {
		List<CustomStore.GetAll> store = storeRepository.findAllNative();
		if(store.size()<=0) {
			return ResponseEntity.internalServerError()
				.body(ErrorResponse.of(HttpStatus.INTERNAL_SERVER_ERROR,
				"Internal server error", "List Of Product is Empty"));
		} else {
			return ResponseEntity.ok(DataResponse.of(HttpStatus.OK, store));
		}
	}
	
	@GetMapping(value="/store/{id}",
		produces = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<ResponseHandler> getStoreById(@PathVariable int id) {
		List<CustomStore.GetAll> store = storeRepository.findByIdNative(id);
		if(store.size()<=0) {
			return ResponseEntity.internalServerError()
				.body(ErrorResponse.of(HttpStatus.INTERNAL_SERVER_ERROR, 
				"Internal server error", "Store is not found"));
		} else {
			return ResponseEntity.ok(DataResponse.of(HttpStatus.OK, store));
		}
	}

	@GetMapping(value = "/store/{name}/products")
	ResponseEntity<ResponseHandler> getStoreProductByName(@PathVariable(name = "name") String storeName) {
		Optional<Store> storeRequest = storeRepository.findByNameNative(storeName);

		if (storeRequest.isPresent()) {
			StoreProductDto storeProductData = null;
			List<CustomProduct.GetAll> getStoreProducts = productRepository.findAllByStoreIdNative(storeRequest.get().getId());

			storeProductData = new StoreProductDto(storeRequest.get(), getStoreProducts);

			return ResponseEntity.ok(DataResponse.of(HttpStatus.OK, storeProductData));
		} else {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(ErrorResponse.of(HttpStatus.NOT_FOUND, "Not Found", "Store name not found"));
		}

		
	}
	
	@PostMapping(value="/store", 
		consumes = MediaType.APPLICATION_JSON_VALUE,
		produces = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<ResponseHandler> createStore(@RequestBody CreateStoreDto createStore) {
		Store storeRequest = createStore.getStore();
		if (createStore == null || storeRequest.getStoreName().length() < 1 || storeRequest.getCity().length() < 1) {
			return ResponseEntity.badRequest().body(ErrorResponse.of(HttpStatus.BAD_REQUEST, "Field Not Fullfilled"));
		}

		Optional<User> user = userRepository.findUserByIdNative(createStore.getId());

		if (user.isEmpty()) {
			return ResponseEntity.badRequest().body(ErrorResponse.of(HttpStatus.NOT_FOUND, "Not Found", "User not found"));
		} 

		Optional<Store> createStoreRequest = storeRepository.findByNameNative(storeRequest.getStoreName());

		if (createStoreRequest.isPresent()) {
			return ResponseEntity.badRequest().body(ErrorResponse.of(HttpStatus.BAD_REQUEST, "Store name already taken", "Provide another name"));
		}


		// update role
		Integer rowUserAffected = userRepository.updateUserRole(createStore.getId(), 2, Instant.now());
		// insert store
		Integer rowStoreAffected = storeRepository.addNewStoreNative(storeRequest.getStoreName(), storeRequest.getCity(), Instant.now(), createStore.getId());

		if (rowUserAffected <= 0 || rowStoreAffected <= 0) {
			return ResponseEntity.internalServerError().body(ErrorResponse.of(HttpStatus.INTERNAL_SERVER_ERROR, "Internal server error", "Error creating store, try again"));
		}

		return ResponseEntity.ok(DataResponse.of(HttpStatus.OK, user.get()));
	}
	
	@DeleteMapping(value="/store/{id}", 
			consumes = MediaType.APPLICATION_JSON_VALUE,
			produces = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<ResponseHandler> softDeleteProduct(@PathVariable int id) {
		int res = storeRepository.softDeleteProductNative(Instant.now(),id);
		if(res ==1) {
			List<CustomStore.GetAll> dataStore = storeRepository.findByIdDeleteNative(id);
			return ResponseEntity.ok(DataResponse.of(HttpStatus.OK, dataStore, 
			"Delete store SuccessFully"));
		} else {
			return ResponseEntity.internalServerError()
				.body(ErrorResponse.of(HttpStatus.INTERNAL_SERVER_ERROR, 
				"Internal server error", "Delete store failed"));
		}
	}
}
