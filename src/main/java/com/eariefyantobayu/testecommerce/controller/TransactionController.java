package com.eariefyantobayu.testecommerce.controller;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

import com.eariefyantobayu.testecommerce.entity.Product;
import com.eariefyantobayu.testecommerce.entity.Transaction;
import com.eariefyantobayu.testecommerce.entity.TransactionDetail;
import com.eariefyantobayu.testecommerce.handler.ResponseHandler;
import com.eariefyantobayu.testecommerce.repository.DetailTransactionRepository;
import com.eariefyantobayu.testecommerce.repository.ProductRepository;
import com.eariefyantobayu.testecommerce.repository.TransactionRepository;
import com.eariefyantobayu.testecommerce.repository.TransactionRepositoryImpl;
import com.eariefyantobayu.testecommerce.util.TransactionStatus;
import com.eariefyantobayu.testecommerce.util.response.DataResponse;
import com.eariefyantobayu.testecommerce.util.response.ErrorResponse;
import com.fasterxml.jackson.annotation.JsonTypeInfo.Id;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class TransactionController {

	@Autowired
	TransactionRepository transactionRepository;

	@Autowired
	TransactionRepositoryImpl transactionRepositoryImpl;

	@Autowired
	ProductRepository productRepository;

	@Autowired
	DetailTransactionRepository detailTransactionRepository;
	
	// get transaction by id
	@GetMapping(value = "/transaction/{id}")
	ResponseEntity<ResponseHandler> getTransactionByid(@PathVariable(value = "id") Integer transactionId) {
		List<Transaction> listTransaction = transactionRepository.findAll();
		return ResponseEntity.ok(DataResponse.of(HttpStatus.OK, listTransaction));
	}

	@GetMapping(value = "/transaction/{id}/details")
	ResponseEntity<ResponseHandler> getTransactionDetailByid(@PathVariable(value = "id") Integer transactionId) {
		List<TransactionDetail> listTransaction = detailTransactionRepository.findAll();
		return ResponseEntity.ok(DataResponse.of(HttpStatus.OK, listTransaction));
	}

	
	// get all transaction details by user id / user
	@GetMapping(value = "/users/{id}/transaction", produces = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<ResponseHandler> getTransactionByUserid(@PathVariable(value = "id") Integer userId) {

		return null;
	}

	@GetMapping(value = "/transaction/test/{storeid}")
	ResponseEntity<ResponseHandler> getTransactionByStoreId(@PathVariable(name = "storeid") Integer storeId) {
		List<Transaction> transactionRequest = transactionRepository.findAllByStoreIdNative(1);

		if (transactionRequest.size() > 0) {
			return ResponseEntity.ok(DataResponse.of(HttpStatus.OK, transactionRequest));
		} else {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(ErrorResponse.of(HttpStatus.INTERNAL_SERVER_ERROR, "Internal Server Error"));
		}

		
	}

	// make transaction batch product
	@PostMapping(value = "/transaction/create/{userId}", 
		consumes = MediaType.APPLICATION_JSON_VALUE,
		produces = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<ResponseHandler> createTransaction(@PathVariable(value = "userId") Integer userId, @RequestBody List<TransactionDetail> listProduct) {
	
			Long lastTransactionId = transactionRepositoryImpl.createTransactionNative(userId);
			
			for (TransactionDetail detail : listProduct) {
				System.out.println("Product : " + detail.getProduct().getId() + ", Qty : " + detail.getQty());
				Optional<Product> productRequest = productRepository.findById(detail.getProduct().getId());
				System.out.println("Present : " + productRequest.isPresent());
				if (productRequest.isPresent()) {
					Product product = productRequest.get();
					detailTransactionRepository.saveNative(
						product.getId(), 
						detail.getQty(), 
						product.getPrice().intValue() * detail.getQty(), 
						lastTransactionId.intValue(), Instant.now());
				} else {
					throw new RuntimeException(("Error"));
					
				}
				
			}
			// insert product each

		return ResponseEntity.ok(DataResponse.of(HttpStatus.OK, "OK"));
	}
	
	// transaction payment status confirmation
	@PostMapping("/transaction/payment/{id}")
	ResponseEntity<ResponseHandler> submitPaymentTransaction(@PathVariable(name = "id") Integer transactionId) {
		Integer lastTransactionId = transactionRepositoryImpl.updateTransactionNative(transactionId, TransactionStatus.PAID);

		return ResponseEntity.ok(DataResponse.of(HttpStatus.OK, lastTransactionId));
	}
	
	// process transaction by seller
	@PostMapping("/transaction/process/{id}")
	ResponseEntity<ResponseHandler> submitProcessTransaction(@PathVariable(name = "id") Integer transactionId) {
		Integer lastTransactionId = transactionRepositoryImpl.updateTransactionNative(transactionId, TransactionStatus.PROCESS);

		return ResponseEntity.ok(DataResponse.of(HttpStatus.OK, lastTransactionId));
	}

	// transaction finish by confirmation by user
	@PostMapping("/transaction/completed/{id}")
	ResponseEntity<ResponseHandler> submitCompleteTransaction(@PathVariable(name = "id") Integer transactionId) {
		Integer lastTransactionId = transactionRepositoryImpl.updateTransactionNative(transactionId, TransactionStatus.FINISH);
		return ResponseEntity.ok(DataResponse.of(HttpStatus.OK, lastTransactionId));
	}
}
