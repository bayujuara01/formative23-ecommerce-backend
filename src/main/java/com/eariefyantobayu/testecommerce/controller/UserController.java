package com.eariefyantobayu.testecommerce.controller;

import java.time.Instant;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;	
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.eariefyantobayu.testecommerce.dto.CustomUser;
import com.eariefyantobayu.testecommerce.entity.User;
import com.eariefyantobayu.testecommerce.handler.PasswordHashHandler;
import com.eariefyantobayu.testecommerce.handler.ResponseHandler;
import com.eariefyantobayu.testecommerce.repository.UserRepository;
import com.eariefyantobayu.testecommerce.util.ObjectMerger;
import com.eariefyantobayu.testecommerce.util.response.DataResponse;
import com.eariefyantobayu.testecommerce.util.response.ErrorResponse;

@RestController
@RequestMapping("/api")
public class UserController {
	
	@Autowired
	UserRepository userRepository;

	
	@GetMapping(value = "/users", 
			produces = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<ResponseHandler> getAllUser() {
		List<CustomUser.GetAll> users = userRepository.findAllNative();
		return ResponseEntity.ok(DataResponse.of(HttpStatus.OK, users));
	}
	
	@GetMapping(value = "/users/{id}")
	ResponseEntity<ResponseHandler> getUserById(@PathVariable Integer id) {
		Optional<CustomUser.GetUserDetail> userDetailRequest = userRepository.findUserDetailById(id);
		ResponseHandler response;
		
		if (userDetailRequest.isPresent()) {
			response = DataResponse.of(HttpStatus.OK, userDetailRequest.get());
		} else {
			response = DataResponse.of(HttpStatus.NOT_FOUND, "User not found");
		}
		
		return ResponseEntity.ok(response);
	}

	@GetMapping(value = "/users/v2/{id}")
	ResponseEntity<ResponseHandler> getUserByIdAuto(@PathVariable Integer id) {
		Optional<User> userDetailRequest = userRepository.findById(id);
		ResponseHandler response;
		
		if (userDetailRequest.isPresent()) {
			response = DataResponse.of(HttpStatus.OK, userDetailRequest.get());
		} else {
			response = DataResponse.of(HttpStatus.NOT_FOUND, "User not found");
		}
		
		return ResponseEntity.ok(response);
	}
	
	@PostMapping(value = "/users", 
			consumes = MediaType.APPLICATION_JSON_VALUE,
			produces = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<ResponseHandler> addNewUser(@Valid @RequestBody User user) {
		Optional<CustomUser.GetByUsername> userRequest = userRepository.findOneByUsernameNative(user.getUsername());
		
		if (userRequest.isPresent()) {
			return ResponseEntity.badRequest().body(ErrorResponse.of(HttpStatus.BAD_REQUEST, "Username already taken", "Provide another username"));
		}
		
		Integer rowAffected = userRepository.saveNative(user.getUsername(), PasswordHashHandler.hashSha256(user.getPassword()), Instant.now()); 
			
		if (rowAffected > 0) {
			return ResponseEntity.ok(DataResponse.of(HttpStatus.OK, userRequest));	
		} else {
			return ResponseEntity.internalServerError()
					.body(ErrorResponse.of(HttpStatus.INTERNAL_SERVER_ERROR, "Insert new user error", "Internal server error"));
		}	
	}
	
	@PutMapping(value = "/users/{id}", 
			consumes = MediaType.APPLICATION_JSON_VALUE,
			produces = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<ResponseHandler> updateUser(@PathVariable Integer id, @RequestBody User user) {
		Optional<User> userRequest = userRepository.findById(id);
		Optional<CustomUser.GetByUsername> usernameRequest = userRepository.findOneByUsernameNative(user.getUsername());
		
		if (usernameRequest.isPresent()) {
			return ResponseEntity.badRequest().body(ErrorResponse.of(HttpStatus.BAD_REQUEST, "Username already taken", "Provide another username"));
		}
		
		User userResult = userRequest.get();
		ObjectMerger.copyNonNullProperties(user, userResult);
		
		Integer rowAffected = userRepository.updateById(userResult.getUsername(), userResult.getPassword(), id, Instant.now());

		if (rowAffected > 0) {
			return ResponseEntity.ok(DataResponse.of(HttpStatus.OK, userRequest));	
		} else {
			return ResponseEntity.internalServerError()
					.body(ErrorResponse.of(HttpStatus.INTERNAL_SERVER_ERROR, "Internal server error", "Update user error"));
		}	
	}	
	
	
}
