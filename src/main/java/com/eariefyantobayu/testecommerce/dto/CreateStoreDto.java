package com.eariefyantobayu.testecommerce.dto;

import javax.validation.constraints.NotEmpty;

import com.eariefyantobayu.testecommerce.entity.Store;

public class CreateStoreDto {
    private Integer id;
    private Store store;

    public CreateStoreDto() {}
    
    public CreateStoreDto(Integer id, Store store) {
        this.id = id;
        this.store = store;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Store getStore() {
        return store;
    }

    public void setStore(Store store) {
        this.store = store;
    }

    
}
