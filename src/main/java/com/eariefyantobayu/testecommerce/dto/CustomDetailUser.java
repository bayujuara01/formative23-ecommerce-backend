package com.eariefyantobayu.testecommerce.dto;

import java.time.Instant;

public interface CustomDetailUser {
	public interface GetAll {
		public Integer getId();
		public String getFullname();
		public String getAddress();
		public String getTelepon();
		public Instant getCreateTime();
		public Instant getUpdateTime();
		public Instant getDeleteTime();
	}
}
