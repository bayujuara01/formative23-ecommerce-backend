package com.eariefyantobayu.testecommerce.dto;

import java.time.Instant;



public interface CustomProduct {
	public interface GetAll {
		public Integer getId();
		public String getProductName();
		public Long getPrice();
		public String getDesc();
		public String getImageUrl();
		public Instant getCreateTime();
		public Instant getUpdateTime();
		public Instant getDeleteTime();
	}
	
	public interface GetName {
		public String getProductName();
	}
}
