package com.eariefyantobayu.testecommerce.dto;

import java.time.Instant;

import org.apache.tomcat.jni.User;

public class CustomStore {
	public interface GetAll {
		public int getId();
		public String getStoreName();
		public String getCity();
		public String getUsername();
		public Instant getCreateTime();
		public Instant getUpdateTime();
		public Instant getDeleteTime(); 
	}
}
