package com.eariefyantobayu.testecommerce.dto;

import com.eariefyantobayu.testecommerce.entity.DetailUser;

public interface CustomUser {
	public interface GetAll {
		public String getUsername();
		public String getPassword();
		public String getTelepon();
	}
	
	public interface GetByUsername {
		public Integer getId();
		public String getUsername();
		public String getPassword();
		public String getRoleName();
		public String getStoreName();
	}
	
	public interface GetHidePassword {
		public String getId();
		public String getUsername();
		// Others
	}
	
	public interface GetLoginInfo {
		public String getId();
		public String getUsername();
	}
	
	public interface GetUserDetail {
		public String getId();
		public String getUsername();
		public String getFullname();
		public String getAddress();
	}
}
