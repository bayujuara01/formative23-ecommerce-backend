package com.eariefyantobayu.testecommerce.dto;

import com.eariefyantobayu.testecommerce.entity.DetailUser;

public interface CustomUserRepository {
	public interface GetAll {
		public String getUsername();
		public String getPassword();
		public String getTelepon();
	}
}
