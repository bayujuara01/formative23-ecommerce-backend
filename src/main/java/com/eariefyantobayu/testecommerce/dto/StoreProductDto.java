package com.eariefyantobayu.testecommerce.dto;

import java.util.List;

import com.eariefyantobayu.testecommerce.entity.Product;
import com.eariefyantobayu.testecommerce.entity.Store;

public class StoreProductDto {
    private Store store;
    private List<CustomProduct.GetAll> products = List.of();
    
    public StoreProductDto(Store store, List<CustomProduct.GetAll> products) {
        this.store = store;
        this.products = products;
    }

    public Store getStore() {
        return store;
    }

    public void setStore(Store store) {
        this.store = store;
    }

    public List<CustomProduct.GetAll> getProducts() {
        return products;
    }

    public void setProducts(List<CustomProduct.GetAll> products) {
        this.products = products;
    }

    
}
