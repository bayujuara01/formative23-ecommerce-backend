package com.eariefyantobayu.testecommerce.entity;

import java.time.Instant;

import javax.persistence.*;

@Table(name = "detail_user")
@Entity
public class DetailUser {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;

    @Column(name = "fullname")
    private String fullname;

    @Column(name = "address")
    private String address;

    @Column(name = "telepon", length = 40)
    private String telepon;
    
    @Column(name = "create_time")
    private Instant createTime;

    
    @Column(name = "update_time")
    private Instant updateTime;
    
    @Column(name = "delete_time")
    private Instant deleteTime;

    public String getTelepon() {
        return telepon;
    }

    public void setTelepon(String telepon) {
        this.telepon = telepon;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

	public Instant getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Instant createTime) {
		this.createTime = createTime;
	}

	public Instant getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Instant updateTime) {
		this.updateTime = updateTime;
	}

	public Instant getDeleteTime() {
		return deleteTime;
	}

	public void setDeleteTime(Instant deleteTime) {
		this.deleteTime = deleteTime;
	}
    
}