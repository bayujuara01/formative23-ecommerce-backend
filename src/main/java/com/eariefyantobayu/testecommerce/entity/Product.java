package com.eariefyantobayu.testecommerce.entity;

import javax.persistence.*;

import java.time.Instant;

@Table(name = "product", indexes = {
        @Index(name = "fk_product_store_idx", columnList = "store_id")
})
@Entity
public class Product{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;

    @Column(name = "product_name")
    private String productName;

    @Column(name = "price")
    private Long price;

    @Lob
    @Column(name = "`desc`")
    private String desc;

    @Column(name = "image_url")
    private String imageUrl;

    @Column(name = "create_time")
    private Instant createTime;

    
    @Column(name = "update_time")
    private Instant updateTime;
    
    @Column(name = "delete_time")
    private Instant deleteTime;

    @ManyToOne(optional = false)
    @JoinColumn(name = "store_id", nullable = false)
    private Store store;

    public Store getStore() {
        return store;
    }

    public void setStore(Store store) {
        this.store = store;
    }

    public Instant getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Instant updateTime) {
        this.updateTime = updateTime;
    }

    public Instant getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Instant createTime) {
        this.createTime = createTime;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

	public Instant getDeleteTime() {
		return deleteTime;
	}

	public void setDeleteTime(Instant deleteTime) {
		this.deleteTime = deleteTime;
	}
    
    
}