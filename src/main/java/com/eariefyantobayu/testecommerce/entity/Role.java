package com.eariefyantobayu.testecommerce.entity;

import javax.persistence.*;

@Table(name = "role")
@Entity
public class Role {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;
    @Column(name = "role_name", nullable = false)
    private String roleName;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    
    public String getRoleName() {
		return roleName;
	}
    
    public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
}