package com.eariefyantobayu.testecommerce.entity;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

import org.hibernate.annotations.DynamicUpdate;
import org.springframework.data.jpa.repository.Temporal;

import java.time.Instant;

@Table(name = "user", indexes = {
        @Index(name = "fk_user_role1_idx", columnList = "role_id")
})
@Entity
@DynamicUpdate
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;

    @Column(name = "username", nullable = false, length = 16)
    @NotEmpty(message = "Username is mandatory")
    private String username;
    
    
    @NotEmpty(message = "Password is mandatory")
    @JsonProperty(access = Access.WRITE_ONLY)
    @Column(name = "password", nullable = false, length = 120)
    private String password;

    @Column(name = "create_time")
    private Instant createTime;

    @Column(name = "update_time")
    private Instant updateTime;
    
    @Column(name = "delete_time")
    private Instant deleteTime;

    @OneToOne(optional = true)
    @JoinColumn(name = "detail_user_id", nullable = true)
    private DetailUser detailUser;

    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    @JoinColumn(name = "role_id", nullable = false)
    private Role role;

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public DetailUser getDetailUser() {
        return detailUser;
    }

    public void setDetailUser(DetailUser detailUser) {
        this.detailUser = detailUser;
    }

    public Instant getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Instant updateTime) {
        this.updateTime = updateTime;
    }

    public Instant getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Instant createTime) {
        this.createTime = createTime;
    }
    
    public Instant getDeleteTime() {
		return deleteTime;
	}
    
    public void setDeleteTime(Instant deleteTime) {
		this.deleteTime = deleteTime;
	}

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}