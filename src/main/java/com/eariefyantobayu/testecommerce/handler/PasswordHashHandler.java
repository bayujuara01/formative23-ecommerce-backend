package com.eariefyantobayu.testecommerce.handler;

import java.nio.charset.StandardCharsets;

import com.google.common.hash.Hashing;

public class PasswordHashHandler {
	public static String hashSha256(String originalString) {
		return Hashing.sha256().hashString(originalString, StandardCharsets.UTF_8).toString();
	}
}
