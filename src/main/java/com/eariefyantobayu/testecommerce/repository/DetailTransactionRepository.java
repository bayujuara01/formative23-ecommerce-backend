package com.eariefyantobayu.testecommerce.repository;

import java.time.Instant;


import com.eariefyantobayu.testecommerce.entity.TransactionDetail;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

public interface DetailTransactionRepository extends JpaRepository<TransactionDetail, Integer> {
    
    @Transactional
    @Modifying
    @Query(value = "INSERT INTO transaction_detail (product_id, qty, total, transaction_id, create_time) " + 
        "VALUES (:productId, :qty, :total, :transactionId, :create);",
        nativeQuery = true)
    Integer saveNative(@Param("productId") Integer productId, 
        @Param("qty") Integer qty, 
        @Param("total") Integer total, 
        @Param("transactionId") Integer transactionId,
        @Param("create") Instant createTime);
}
