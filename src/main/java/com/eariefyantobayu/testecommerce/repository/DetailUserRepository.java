package com.eariefyantobayu.testecommerce.repository;

import java.time.Instant;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.eariefyantobayu.testecommerce.entity.DetailUser;
import com.eariefyantobayu.testecommerce.dto.CustomDetailUser;

public interface DetailUserRepository extends JpaRepository<DetailUser, Integer>{
	@Query(value="SELECT detail_user.id, detail_user.fullname, detail_user.address, "
		+ "detail_user.telepon, detail_user.create_time as createTime, detail_user.update_time as updateTime, "
		+ "detail_user.delete_time FROM detail_user WHERE detail_user.delete_time IS NULL" 
		,nativeQuery = true)
	List<CustomDetailUser.GetAll> findAllNative();
	
	@Query(value="SELECT detail_user.id, detail_user.fullname, detail_user.address, "
		+ "detail_user.telepon, detail_user.create_time as createTime, detail_user.update_time as updateTime, "
		+ "detail_user.delete_time as deleteTime FROM detail_user WHERE detail_user.id=?1 and "
		+ "detail_user.delete_time IS NULL" ,nativeQuery = true)
	List<CustomDetailUser.GetAll> findByIdNative(int id);
	
	@Query(value="SELECT detail_user.id, detail_user.fullname, detail_user.address, "
		+ "detail_user.telepon, detail_user.create_time as createTime, detail_user.update_time as updateTime, "
		+ "detail_user.delete_time as deleteTime FROM detail_user WHERE detail_user.delete_time IS NULL "
		+ "ORDER BY id DESC LIMIT 1" ,nativeQuery = true)
	List<CustomDetailUser.GetAll> findLastOfListNative();
	
	@Transactional
	@Modifying
	@Query(value="INSERT into detail_user (fullname, address, telepon, create_time) "
		+ "VALUES (?1,?2,?3,?4)",nativeQuery = true)
	int addNewDetailUserNative(String fullName, String address, String telepon, Instant createTime);
	
	@Transactional
	@Modifying
	@Query(value="UPDATE detail_user SET delete_time=?1 WHERE id=?2",nativeQuery = true)
	int softDeleteDetailUserNative(Instant currentTime, int id);
	
	@Query(value="SELECT detail_user.id, detail_user.fullname, detail_user.address, "
		+ "detail_user.telepon, detail_user.create_time as createTime, detail_user.update_time as updateTime, "
		+ "detail_user.delete_time as deleteTime FROM detail_user WHERE detail_user.id=?1" 
		,nativeQuery = true)
	List<CustomDetailUser.GetAll>  findByIdDeleteNative(int id);
	
	@Transactional
	@Modifying
	@Query(value="UPDATE detail_user SET fullname=?1,address =?2, telepon=?3, update_time=?4 "
		+ "WHERE id=?5",nativeQuery = true)
	int updateByIdNative(String fullName, String address, String telepon, Instant createTime,
		int id);
	
}
