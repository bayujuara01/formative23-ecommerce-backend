package com.eariefyantobayu.testecommerce.repository;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.eariefyantobayu.testecommerce.dto.CustomProduct;
import com.eariefyantobayu.testecommerce.dto.CustomProduct.GetAll;
import com.eariefyantobayu.testecommerce.entity.Product;

public interface ProductRepository  extends JpaRepository<Product, Integer>{

	@Query(value = "SELECT product.id, product.product_name as productName, product.price, "
		+ "product.desc, product.image_url as imageUrl, product.create_time as createTime, "
		+ "product.update_time as updateTime , product.delete_time as deleteTime"
		+ "  FROM product WHERE product.delete_time is null",nativeQuery = true)
	List<CustomProduct.GetAll> findAllNative();
	
	@Query(value = "SELECT product.id, product.product_name as productName, product.price, "
		+ "product.desc, product.image_url as imageUrl, product.create_time as createTime, "
		+ "product.update_time as updateTime  FROM product WHERE id=?1 AND product.delete_time is null"
		,nativeQuery = true)
	List<CustomProduct.GetAll> findByIdNative(int id);

	@Transactional
	@Query(value = "SELECT * FROM product WHERE id=?1 AND product.delete_time is null",
		nativeQuery = true)
	Optional<Product> findOneByIdNative(Integer id);
	
	@Query(value ="select product.id, product.product_name as productName, product.price, "
		+ "product.desc, product.image_url as imageUrl, product.create_time as createTime,"
		+ " product.update_time as updateTime from product ORDER BY id DESC LIMIT 1",
		nativeQuery = true)
	List<CustomProduct.GetAll> findLastOfListNative();
	
	@Query(value = "SELECT product.id, product.product_name as productName, product.price, "
	+ "product.desc, product.image_url as imageUrl, product.create_time as createTime, "
	+ "product.update_time as updateTime , product.delete_time as deleteTime"
	+ "  FROM product WHERE product.store_id = ?1 AND product.delete_time is null",nativeQuery = true)
	List<CustomProduct.GetAll> findAllByStoreIdNative(Integer storeId);
	
	@Transactional
	@Modifying
	@Query(value="INSERT INTO product (product_name, price, `desc`, image_url, create_time, "
		+ "store_id) VALUES (?1,?2,?3,?4,?5,?6)",nativeQuery = true)
	int addNewProductNative(String productName, Long price, String desc, String imageUrl, 
		Instant currentTime, int id);
	
	@Transactional
	@Modifying
	@Query(value="UPDATE product SET delete_time=?1 WHERE id=?2",nativeQuery = true)
	int softDeleteProductNative(Instant deleteTime, int id);
	
	@Query(value = "SELECT product.id, product.product_name as productName, product.price, "
		+ "product.desc, product.image_url as imageUrl, product.create_time as createTime, "
		+ "product.update_time as updateTime ,product.delete_time as deleteTime  FROM product WHERE id=?1"
		,nativeQuery = true)
	List<CustomProduct.GetAll> findByIdDeleteNative(int id);
	
	@Transactional
	@Modifying
	@Query(value="UPDATE product SET product_name = :name, price = :price , `desc`= :description, image_url= :image, update_time = :time WHERE id = :id",nativeQuery = true)
	Integer updateById(@Param("name") String productName,@Param("price") Long price, @Param("description") String desc,@Param("image") String imageUrl, 
		@Param("time") Instant now ,@Param("id") int id);
//	@Transactional
//	@Modifying
//	@Query(value="UPDATE product SET product_name = :name WHERE id = :id",nativeQuery = true)
//	Integer updateById(@Param("name") String productName, @Param("id") int id);
	
	@Transactional
	@Modifying
	@Query(value="UPDATE product SET product_name='Xiaomi Redmi Note 10 Pro d' WHERE id=2",nativeQuery = true)
	Integer updateByIdOne();
	
	

	@Query(value = "SELECT product.id, product.product_name as productName, product.price, "
			+ "product.desc, product.image_url as imageUrl, product.create_time as createTime, "
			+ "product.update_time as updateTime  FROM product WHERE product.product_name LIKE %?1%"
			+ "OR product.desc LIKE %?2% OR product.price=?3"
			,nativeQuery = true)
	List<CustomProduct.GetAll> findByParameter(String productName, String desc, int price);


}
