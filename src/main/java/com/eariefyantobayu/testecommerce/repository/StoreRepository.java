package com.eariefyantobayu.testecommerce.repository;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.eariefyantobayu.testecommerce.entity.Store;
import com.eariefyantobayu.testecommerce.dto.CustomStore;

public interface StoreRepository extends JpaRepository<Store, Integer>{
	
	@Query(value="SELECT store.id, store.store_name as storeName, store.city , "
		+ "store.create_time as createTime,store.update_time as updateTime , "
		+ "store.delete_time as deleteTime , user.username , user.id as userId "
		+ "FROM store JOIN user ON user.id =store.user_id WHERE store.delete_time is null",
		nativeQuery = true)
	List<CustomStore.GetAll> findAllNative();
	
	@Query(value="SELECT store.id, store.store_name as storeName, store.city , "
		+ "store.create_time as createTime,store.update_time as updateTime , "
		+ "store.delete_time as deleteTime , user.username , user.id as userId "
		+ "FROM store JOIN user ON user.id =store.user_id WHERE store.id=?1 AND"
		+ "  store.delete_time is null",nativeQuery = true)
	List<CustomStore.GetAll> findByIdNative(int id);

	@Query(value = "SELECT store.id, store.store_name, store.city, store.create_time, "
	+ " store.update_time, store.delete_time, store.user_id "
	+ "FROM store WHERE store.store_name = ?1",
		nativeQuery = true)
	Optional<Store> findByNameNative(String storeName);

	@Query(value = "SELECT store.id, store.store_name as storeName, store.city , "
	+ "store.create_time as createTime,store.update_time as updateTime , "
	+ "store.delete_time as deleteTime , user.username , user.id as userId "
	+ "FROM store JOIN user ON user.id =store.user_id WHERE store.user_id = ?1",
		nativeQuery = true)
	Optional<CustomStore.GetAll> findByUserIdNative(Integer id);
	
	@Query(value="SELECT store.id, store.store_name as storeName, store.city , "
		+ "store.create_time as createTime,store.update_time as updateTime , "
		+ "store.delete_time as deleteTime , user.username , user.id as userId "
		+ "FROM store JOIN user ON user.id =store.user_id ORDER BY id DESC LIMIT 1",
	nativeQuery = true)
	List<CustomStore.GetAll> findLastOfListNative();
	
	@Transactional
	@Modifying
	@Query(value="INSERT INTO store (store_name, city, create_time, user_id) "
		+ "VALUES (?1,?2,?3,?4)",nativeQuery = true)
	int addNewStoreNative(String storeName, String city, Instant currentTime,	int userId);
	
	
	@Transactional
	@Modifying
	@Query(value="UPDATE store SET delete_time=?1 WHERE id=?2",nativeQuery = true)
	int softDeleteProductNative(Instant deleteTime, int id);
	
	@Query(value="SELECT store.id, store.store_name as storeName, store.city , "
		+ "store.create_time as createTime,store.update_time as updateTime , "
		+ "store.delete_time as deleteTime , user.username , user.id as userId "
		+ "FROM store JOIN user ON user.id =store.user_id WHERE store.id=?1 "
		,nativeQuery = true)
	List<CustomStore.GetAll> findByIdDeleteNative(int id);
	
}
