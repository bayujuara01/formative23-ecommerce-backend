package com.eariefyantobayu.testecommerce.repository;

import java.util.List;
import java.util.Optional;

import com.eariefyantobayu.testecommerce.entity.Transaction;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface TransactionRepository extends JpaRepository<Transaction, Integer> {
    @Query(value = "SELECT DISTINCT transaction.id, transaction.`status`, transaction.grand_total, transaction.user_id, transaction.create_time, transaction.update_time FROM transaction " +
        "JOIN transaction_detail ON transaction.id = transaction_detail.transaction_id " +
        "JOIN product ON product.id = transaction_detail.product_id " +
        "JOIN store ON store.id = product.store_id WHERE store.id = ?1",
        nativeQuery = true)
     List<Transaction> findAllByStoreIdNative(Integer storeId);

     @Query(value = "SELECT transaction.id, transaction.`status`, transaction.grand_total, transaction.user_id, transaction.create_time, transaction.update_time FROM transaction " +
     "JOIN transaction_detail ON transaction.id = transaction_detail.transaction_id " +
     "JOIN product ON product.id = transaction_detail.product_id " +
     "JOIN store ON store.id = product.store_id WHERE transaction.user_id = ?1",
     nativeQuery = true)
  List<Transaction> findAllByUserIdNative(Integer userId);
}
