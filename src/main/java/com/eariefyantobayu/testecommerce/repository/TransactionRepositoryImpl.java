package com.eariefyantobayu.testecommerce.repository;

import java.math.BigInteger;
import java.time.Instant;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.Transactional;

import com.eariefyantobayu.testecommerce.util.TransactionStatus;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Repository;

@Repository
public class TransactionRepositoryImpl {
    
    @Autowired
    EntityManager entityManager;

    @Transactional
    public Long createTransactionNative(Integer userId) {
        BigInteger lastInsertId = BigInteger.valueOf(-1);

        // entityManager.getTransaction().begin();
        Query query = entityManager.createNativeQuery(
            "INSERT INTO transaction(status, grand_total, user_id, create_time) VALUES ('UNPAID', :grandtotal, :userid, :create);");
        query.setParameter("grandtotal", 0);
        query.setParameter("userid", userId);
        query.setParameter("create", Instant.now());
        query.executeUpdate();

        try {
            Query queryLastInsertId = entityManager.createNativeQuery("SELECT last_insert_id();");
            lastInsertId = (BigInteger) queryLastInsertId.getSingleResult();
            // entityManager.getTransaction().commit();
        } catch (Exception e) {
            // entityManager.getTransaction().rollback();
            lastInsertId = BigInteger.valueOf(-1L);
        }

        return lastInsertId.longValue();
    }

    @Transactional
    // @Modifying
    public Integer updateTransactionNative(Integer transactionId, TransactionStatus status) {

        // entityManager.getTransaction().begin();
        Query query = entityManager.createNativeQuery(
            "UPDATE transaction SET status = :status WHERE id = :id");
        query.setParameter("status", status.value());
        query.setParameter("id", transactionId);
        query.executeUpdate();


        return transactionId;
    }
}
