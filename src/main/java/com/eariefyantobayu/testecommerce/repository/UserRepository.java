package com.eariefyantobayu.testecommerce.repository;

import com.eariefyantobayu.testecommerce.dto.CustomUser;
import com.eariefyantobayu.testecommerce.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Integer> {

	// TEST
	@Query(value = "SELECT user.username, user.password, detail_user.telepon FROM user JOIN detail_user ON user.detail_user_id = detail_user.id", nativeQuery = true)
	List<CustomUser.GetAll> findAllNative();

	@Query(value = "SELECT user.id, user.username, user.password, user.detail_user_id, user.role_id ,user.create_time, user.update_time, user.delete_time FROM user WHERE id = ?1", nativeQuery = true)
	Optional<User> findUserByIdNative(Integer id);

	@Query(value = "SELECT user.id, user.username, detail_user.fullname, detail_user.address, detail_user.telepon FROM user JOIN detail_user ON user.detail_user_id = detail_user.id WHERE user.id = ?1", nativeQuery = true)
	Optional<CustomUser.GetUserDetail> findUserDetailById(Integer userId);

	@Query(value = "SELECT id, username FROM user WHERE id = ?!", nativeQuery = true)
	Optional<CustomUser.GetLoginInfo> findLoginInfoById(Integer id);

	@Query(value = "SELECT user.id, user.username, user.password, role.role_name as roleName, " + 
		" store.store_name as storeName FROM user JOIN role ON user.role_id = role.id LEFT JOIN store ON user.id = store.user_id WHERE username = ?1", nativeQuery = true)
	Optional<CustomUser.GetByUsername> findOneByUsernameNative(String username);

	@Transactional
	@Modifying
	@Query(value = "INSERT INTO user(username, password, role_id, create_time) VALUES (:username, :password, 1, :create)", nativeQuery = true)
	Integer saveNative(@Param("username") String username, @Param("password") String password, @Param("create") Instant create);

	@Transactional
	@Modifying
	@Query(value = "UPDATE user SET user.role_id = :role, user.update_time = :update WHERE user.id = :id",
	 	nativeQuery = true)
	Integer updateUserRole(@Param("id") Integer userId,@Param("role") Integer roleId, @Param("update") Instant updateTime);

	@Transactional
	@Modifying
	@Query(value = "UPDATE user SET username = :username, password = :password, update_time = :time WHERE id = :id", nativeQuery = true)
	Integer updateById(@Param("username") String username, @Param("password") String password,
			@Param("id") Integer userId, @Param("time") Instant updateTime);
}