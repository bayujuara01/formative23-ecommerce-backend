package com.eariefyantobayu.testecommerce.util;

public enum TransactionStatus {
    UNPAID("UNPAID"),
    PAID("PAID"),
    CANCEL("CANCEL"),
    PROCESS("PROCESS"),
    ACCEPT("ACCEPT"),
    FINISH("FINISH");

    private String value;

    TransactionStatus(String value) {
        this.value = value;
    }

    public String value() {
        return value;
    }
}
