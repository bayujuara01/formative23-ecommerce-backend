package com.eariefyantobayu.testecommerce.util.response;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import com.eariefyantobayu.testecommerce.handler.ResponseHandler;

public class DataResponse implements ResponseHandler {
	private int status;
	private Object data;
	private String message;
	
	public int getStatus() {
		return status;
	}
	
	public Object getData() {
		return data;
	}
	
	public String getMessage() {
		return message;
	}
	
	public DataResponse(int status, Object data, String message) {
		this.status = status;
		this.data = data;
		this.message = message;
	}
	
	public static DataResponse of(HttpStatus httpStatus, Object data, String message) {
		return new DataResponse(httpStatus.value(), data, message);
	}
	
	public static DataResponse of(HttpStatus httpStatus, Object data) {
		return new DataResponse(httpStatus.value(), data, "-");
	}
}
