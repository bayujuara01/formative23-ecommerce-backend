package com.eariefyantobayu.testecommerce.util.response;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import com.eariefyantobayu.testecommerce.handler.ResponseHandler;

public class ErrorResponse implements ResponseHandler {
	private int status;
	private Object errors;
	private String message;
	
	public int getStatus() {
		return status;
	}
	
	public Object getErrors() {
		return errors;
	}
	
	public String getMessage() {
		return message;
	}
	
	public ErrorResponse(int status, Object errors, String message) {
		this.status = status;
		this.errors = errors;
		this.message = message;
	}
	
	public static ErrorResponse of(HttpStatus httpStatus, List<String> errors, String message) {
		return new ErrorResponse(httpStatus.value(), errors, message);
	}
	
	public static ErrorResponse of(HttpStatus httpStatus, String errors, String message) {
		return new ErrorResponse(httpStatus.value(), List.of(errors), message);
	}
	
	public static ErrorResponse of(HttpStatus httpStatus, List<String> errors) {
		return new ErrorResponse(httpStatus.value(), errors, "-");
	}
	
	public static ErrorResponse of(HttpStatus httpStatus, String errors) {
		return new ErrorResponse(httpStatus.value(), List.of(errors), "-");
	}
	
	public static ErrorResponse of(HttpStatus httpStatus, Object errors) {
		return new ErrorResponse(httpStatus.value(), errors, "-");
	}
}
